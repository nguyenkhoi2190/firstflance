package busttle.busttleapp.com.busttle.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import busttle.busttleapp.com.busttle.R;
import busttle.busttleapp.com.busttle.common.ApiUrl;
import busttle.busttleapp.com.busttle.model.DataManager;
import busttle.busttleapp.com.busttle.model.Parse;
import busttle.busttleapp.com.busttle.utils.Connectivity;
import busttle.busttleapp.com.busttle.utils.MyJsonRequest;
import busttle.busttleapp.com.busttle.utils.Utils;

public class MainActivity extends ActionBarActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private Button btnLogin;
    private Button btnSetting;
    private EditText etDriverId;
    private EditText etPassword;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.dialog = new Dialog(this);
        this.btnLogin = (Button) findViewById(R.id.btn_login_login);
        this.btnSetting = (Button) findViewById(R.id.btn_login_setting);
        this.etDriverId = (EditText) findViewById(R.id.et_login_driver_id);
        this.etPassword = (EditText) findViewById(R.id.et_login_password);

        // Set OnClickListener
        this.btnLogin.setOnClickListener(loginButtonOnClickListener);
        this.btnSetting.setOnClickListener(settingButtonOnClickListener);


        // set value
        etDriverId.setText(DataManager.getInstance().getDriver(this).getEmail());
        etPassword.setText("abc123");


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dialog !=null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    public void executeLogin() {
        if (!Connectivity.isConnected(this)) {
           return;
        }

        final String email = etDriverId.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();

        MyJsonRequest jsonRequest = new MyJsonRequest(Request.Method.POST, ApiUrl.LOGIN, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                // TODO: Parse user Model
                if (jsonObject.has("driver")) {
                    JSONObject driverObject = Parse.getJSONObject(jsonObject, "driver");
                    DataManager.getInstance().saveDriver(MainActivity.this, driverObject);
                    Intent statusListIntent = new Intent(MainActivity.this, BookingListActivity.class);
                    MainActivity.this.startActivity(statusListIntent);
                    finish();
                } else if (jsonObject.has("error")) {
                    String errorString = Parse.getString(jsonObject, "error");
                    Toast.makeText(MainActivity.this, errorString, Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(MainActivity.this, "Server not respond", Toast.LENGTH_SHORT).show();
                }

//                Gson gson = new Gson();
//                Driver driver = null;
//                try {
//                    driver = gson.fromJson(jsonObject.get("driver").toString(), Driver.class);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                Intent statusListIntent = new Intent(MainActivity.this, BookingListActivity.class);
//                MainActivity.this.startActivity(statusListIntent);
//                Log.i(TAG, jsonObject.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.i(TAG, volleyError.toString());
            Toast.makeText(MainActivity.this, "Invalid email or password", Toast.LENGTH_SHORT).show();
            }

        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };

        MainApplication.getInstance().addToRequestQueue(jsonRequest);

    }

    private View.OnClickListener loginButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            executeLogin();
        }
    };

    private View.OnClickListener settingButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO: show pop up admin login


            dialog.setContentView(R.layout.custom_admin_login_dialog);
            dialog.setTitle("Admin Login");

            final EditText editText=(EditText)dialog.findViewById(R.id.et_login_admin_password);
            Button login = (Button)dialog.findViewById(R.id.btn_login_admin_login);
            Button cancel = (Button)dialog.findViewById(R.id.btn_login_admin_cancel);

            dialog.show();

            // Set onclick listener
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO: Move to update setting activity
                    dialog.hide();
                    String password = editText.getText().toString();
                    if (Utils.md5(password).equals(DataManager.getInstance().getAdminPassword(MainActivity.this))) {
                        Intent settingIntent = new Intent(MainActivity.this, SettingActivity.class);
                        MainActivity.this.startActivity(settingIntent);
                    } else {

                    }

                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.hide();
                }
            });
        }
    };


}


