package busttle.busttleapp.com.busttle.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bom on 3/22/2015.
 */
public class Parse {
    private static final String TAG = Parse.class.getSimpleName();

    public static String getString(JSONObject jsonObject, String key) {
        String result = null;

        if (jsonObject != null && !jsonObject.isNull(key)) {
            try {
                result = jsonObject.getString(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static boolean getBoolean(JSONObject jsonObject, String key) {
        boolean result = false;

        if (jsonObject != null && !jsonObject.isNull(key)) {
            try {
                result = jsonObject.getBoolean(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static int getInt(JSONObject jsonObject, String key) {
        int result = 0;

        if (jsonObject != null && !jsonObject.isNull(key)) {
            try {
                result = jsonObject.getInt(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static double getDouble(JSONObject jsonObject, String key) {
        double result = 0;

        if (jsonObject != null && !jsonObject.isNull(key)) {
            try {
                result = jsonObject.getDouble(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static JSONObject getJSONObject(JSONObject jsonObject, String key) {
        JSONObject result = null;

        if (jsonObject != null && !jsonObject.isNull(key)) {
            try {
                result = jsonObject.getJSONObject(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static JSONArray getJSONArray(JSONObject jsonObject, String key) {
        JSONArray result = null;

        if (jsonObject != null && !jsonObject.isNull(key)) {
            try {
                result = jsonObject.getJSONArray(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }
}
