package busttle.busttleapp.com.busttle.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import busttle.busttleapp.com.busttle.R;
import busttle.busttleapp.com.busttle.adapter.BookingListViewAdapter;
import busttle.busttleapp.com.busttle.common.ApiUrl;
import busttle.busttleapp.com.busttle.model.BusttleItem;
import busttle.busttleapp.com.busttle.model.DataManager;
import busttle.busttleapp.com.busttle.service.RequestService;
import busttle.busttleapp.com.busttle.utils.Connectivity;
import busttle.busttleapp.com.busttle.utils.MyJsonRequest;

public class BookingListActivity extends ActionBarActivity {

    public static final String TAG = BookingListActivity.class.getSimpleName();
    private ListView bookingListView;
    private BookingListViewAdapter mAdapter;
    private ArrayList<BusttleItem> busttleItemsArrayList;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");

        // Init Mock Data
        busttleItemsArrayList = new ArrayList<BusttleItem>();
        for (int i = 0; i < 20; i++) {
            BusttleItem item = new BusttleItem("Name " + i,"Mobile " + i,"PickUp "+ i,"Drop Off "+ i,1);
            busttleItemsArrayList.add(item);
        }
        this.bookingListView = (ListView) this.findViewById(R.id.lv_list);
        mAdapter = new BookingListViewAdapter(this, R.layout.info_list_row_normal, busttleItemsArrayList);
        bookingListView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshBookingList();
        startRequestService();
    }

    public void refreshBookingList() {
        if (!Connectivity.isConnected(this)) {
            return;
        }
        DataManager dataManager = DataManager.getInstance();
        String url = ApiUrl.GETLIST + "bus_id=" + dataManager.getBusId(this) + "&authentication_token=" + dataManager.getDriver(this).getAccessToken();
        Log.i(TAG, url.toString());
        MyJsonRequest jsonRequest = new MyJsonRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.i(TAG, jsonObject.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.i(TAG, volleyError.toString());


            }

        });

        MainApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void startRequestService() {
        Log.i(TAG, "Start Service");
        startService(new Intent(this, RequestService.class));
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(this, RequestService.class);
        intent.putExtra("message", "Send Request");
        PendingIntent pintent = PendingIntent
                .getService(this, 0, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // Start service every hour
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                10*1000, pintent);
    }

    public void execute(final String bookingId, final String requestAction) {
        if (!Connectivity.isConnected(this)) {
            return;
        }

        String url = ApiUrl.UPDATE_BOOKING;
        MyJsonRequest jsonRequest = new MyJsonRequest(Request.Method.POST, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d(TAG, jsonObject.toString());
                mProgressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onErrorResponse");
                mProgressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("booking_id ", bookingId);
                params.put("bus_id ", DataManager.getInstance().getBusId(BookingListActivity.this));
                params.put("authentication_token", DataManager.getInstance()
                        .getDriver(BookingListActivity.this).getAccessToken());
                params.put("request_action", requestAction);
                return params;
            }
        };

        mProgressDialog.show();
        MainApplication.getInstance().addToRequestQueue(jsonRequest);

    }

}
