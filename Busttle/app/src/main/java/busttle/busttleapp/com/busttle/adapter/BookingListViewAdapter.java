package busttle.busttleapp.com.busttle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import busttle.busttleapp.com.busttle.R;
import busttle.busttleapp.com.busttle.model.BusttleItem;

public class BookingListViewAdapter extends ArrayAdapter<BusttleItem> implements View.OnClickListener {

    private ArrayList<BusttleItem> mData = new ArrayList<BusttleItem>();
    private LayoutInflater mInflater;

    public BookingListViewAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public BookingListViewAdapter(Context context, int resource, ArrayList<BusttleItem> items) {
        super(context, resource, items);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mData = items;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public BusttleItem getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.info_list_row_normal, null);
            holder = new ViewHolder();
            holder.tvName = (TextView)convertView.findViewById(R.id.tv_list_cell_name);
            holder.tvMobileNumber = (TextView)convertView.findViewById(R.id.tv_list_cell_mobile);
            holder.tvPickedUp = (TextView)convertView.findViewById(R.id.tv_list_cell_pick_up);
            holder.tvDropOff = (TextView)convertView.findViewById(R.id.tv_list_cell_drop_off);
            holder.btnPickedUp = (Button)convertView.findViewById(R.id.btn_list_cell_picked_up);
            holder.btnNoShow = (Button)convertView.findViewById(R.id.btn_list_cell_no_show);
            holder.btnDropOff = (Button)convertView.findViewById(R.id.btn_list_cell_drop_off);
            holder.container = (LinearLayout) convertView.findViewById(R.id.container_list_cell);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        BusttleItem itemData = mData.get(position);
        holder.tvName.setText(itemData.getName());
        holder.tvMobileNumber.setText(itemData.getMobileNumber());
        holder.tvPickedUp.setText(itemData.getPickUp());
        holder.tvDropOff.setText(itemData.getDropOff());
        holder.btnPickedUp.setOnClickListener(pickUpOnClickListener);
        holder.btnNoShow.setOnClickListener(noShowOnClickListener);
        if (position % 2 == 1) {
            holder.container.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        } else {
            holder.container.setBackgroundColor(getContext().getResources().getColor(R.color.green));
        }

        // set is picked up
        int status = itemData.getStatus();
        switch (status) {
            case 1:
                holder.btnPickedUp.setVisibility(View.VISIBLE);
                holder.btnNoShow.setVisibility(View.VISIBLE);
                holder.btnDropOff.setVisibility(View.GONE);
                break;

            case 2:
                holder.btnPickedUp.setVisibility(View.GONE);
                holder.btnNoShow.setVisibility(View.GONE);
                holder.btnDropOff.setVisibility(View.VISIBLE);
                break;

            default:
                holder.btnPickedUp.setVisibility(View.VISIBLE);
                holder.btnNoShow.setVisibility(View.VISIBLE);
                holder.btnDropOff.setVisibility(View.GONE);
                break;

        }

        holder.btnPickedUp.setOnClickListener(this);

        // set tag
        holder.btnPickedUp.setTag(position);

        return convertView;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.btn_list_cell_picked_up) {
            int position = (int) v.getTag();
            getItem(position).setStatus(2);
            notifyDataSetChanged();
        }

    }


    public static class ViewHolder {
        public TextView tvName;
        public TextView tvMobileNumber;
        public TextView tvPickedUp;
        public TextView tvDropOff;
        public Button btnPickedUp;
        public Button btnNoShow;
        public Button btnDropOff;
        public LinearLayout container;
    }

    private View.OnClickListener pickUpOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private View.OnClickListener noShowOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };
}
