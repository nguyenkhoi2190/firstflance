package busttle.busttleapp.com.busttle.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import busttle.busttleapp.com.busttle.R;
import busttle.busttleapp.com.busttle.common.ApiUrl;
import busttle.busttleapp.com.busttle.model.DataManager;
import busttle.busttleapp.com.busttle.model.Parse;
import busttle.busttleapp.com.busttle.utils.Connectivity;
import busttle.busttleapp.com.busttle.utils.MyJsonRequest;

public class SettingActivity extends ActionBarActivity implements TextView.OnEditorActionListener {

    public static final String TAG = SettingActivity.class.getSimpleName();

    private Button btnUpdate;
    private Button btnDone;
    private EditText etVehicleId;
    private TextView tvVehicleName;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(true);

        // Set up Views
        btnUpdate = (Button) findViewById(R.id.btn_setting_update);
        btnDone = (Button) findViewById(R.id.btn_setting_done);
        etVehicleId = (EditText) findViewById(R.id.et_setting_vehicle_id);
        tvVehicleName = (TextView) findViewById(R.id.tv_setting_vehicle_name);

        // Set Listener
        btnDone.setOnClickListener(doneButtonClickListener);
        btnUpdate.setOnClickListener(updateButtonClickListener);

        etVehicleId.setOnEditorActionListener(this);
    }



    private View.OnClickListener doneButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private View.OnClickListener updateButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // TODO: Update Request
            executeSetting();
        }
    };

    private void executeSetting() {
        if (!Connectivity.isConnected(this)) {
            return;
        }

        final String vehicleId = etVehicleId.getText().toString().trim();
        String url = ApiUrl.SETTING + "?bus_id=" + vehicleId + "&authentication_token=" + DataManager.getInstance().getDriver(this).getAccessToken();
        MyJsonRequest jsonRequest = new MyJsonRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject != null && jsonObject.has("name")) {
                    String name = Parse.getString(jsonObject, "name");
                    tvVehicleName.setText(name);
                    DataManager.getInstance().saveBusId(SettingActivity.this, vehicleId);
                    finish();
                } else {
                    tvVehicleName.setText("Vehicle not found");
                }

                mProgressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                tvVehicleName.setText("Vehicle not found");
                Log.i(TAG, volleyError.toString());
                mProgressDialog.dismiss();
            }


        });

        mProgressDialog.show();
        MainApplication.getInstance().addToRequestQueue(jsonRequest);

    }

    public void executeGettingName() {
        if (!Connectivity.isConnected(this)) {
            Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT).show();
        }

        final String busId = etVehicleId.getText().toString().trim();
        String token = DataManager.getInstance().getDriver(this).getAccessToken();
        String url = ApiUrl.SETTING + "?bus_id=" + busId + "&authentication_token=" + token;

        MyJsonRequest jsonRequest = new MyJsonRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.i(TAG, jsonObject.toString());
                if (jsonObject != null) {
                    String name = Parse.getString(jsonObject, "name");
                    tvVehicleName.setText(name);
                    DataManager.getInstance().saveBusId(SettingActivity.this, busId);
                } else {
                    tvVehicleName.setText("Vehicle not found");
                }

                mProgressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                tvVehicleName.setText("Vehicle not found");
                mProgressDialog.dismiss();
            }
        });

        mProgressDialog.show();
        MainApplication.getInstance().addToRequestQueue(jsonRequest);
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            executeGettingName();
            return true;
        }
        return false;
    }
}

