package busttle.busttleapp.com.busttle.common;

public class ApiUrl {

    public static final String HOST = "http://staging01.int.busttle.com/api/v1/";

    public static final String LOGIN = HOST + "drivers/sign_in";

    public static final String GETLIST = HOST + "booking_driver/refresh?";

    public static final String SETTING = HOST + "booking_driver/setting";

    public static final String DRIVER = HOST + "drivers/";

    public static final String UPDATE_BOOKING = HOST + "booking_driver/update_booking";
}
