package busttle.busttleapp.com.busttle.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import busttle.busttleapp.com.busttle.activities.MainApplication;
import busttle.busttleapp.com.busttle.common.ApiUrl;
import busttle.busttleapp.com.busttle.model.DataManager;
import busttle.busttleapp.com.busttle.utils.Connectivity;
import busttle.busttleapp.com.busttle.utils.MyJsonRequest;

public class RequestService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, " MyService Created ", Toast.LENGTH_LONG).show();
        Log.i("Service", "Create");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("Service", "Start");
        String message = intent.getStringExtra("message");
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        return Service.START_STICKY;
    }

    public void refreshBookingList() {
        if (!Connectivity.isConnected(this)) {
            return;
        }
        DataManager dataManager = DataManager.getInstance();
        String url = ApiUrl.GETLIST + "bus_id=" + dataManager.getBusId(this) + "&authentication_token=" + dataManager.getDriver(this).getAccessToken();

        MyJsonRequest jsonRequest = new MyJsonRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }

        });

        MainApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        Toast.makeText(this, "Servics Stopped", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

}
