package busttle.busttleapp.com.busttle.model;

import org.json.JSONObject;

public class BusttleItem {

    private String id;
    private String name;
    private String mobileNumber;
    private String pickUp;
    private String dropOff;
    private String pickupTime;
    private String requestTime;
    private String dropoutTime;
    private int status;

    public BusttleItem(JSONObject jsonObject) {
        if (jsonObject != null) {
            id = Parse.getString(jsonObject, "id");
            name = Parse.getString(jsonObject, "user_name");
            mobileNumber = Parse.getString(jsonObject, "user_phone  ");
            pickUp = Parse.getString(jsonObject, "from_location");
            dropOff = Parse.getString(jsonObject, "to_location");
            pickupTime = Parse.getString(jsonObject, "pickup_time");
            requestTime = Parse.getString(jsonObject, "request_time");
            dropoutTime = Parse.getString(jsonObject, "dropout_time");
            status = Parse.getInt(jsonObject, "status");
        }
    }

    public BusttleItem(String name, String mobileNumber, String pickUp, String dropOff, int status) {
        this.name = name;
        this.mobileNumber = mobileNumber;
        this.pickUp = pickUp;
        this.dropOff = dropOff;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPickUp() {
        return pickUp;
    }

    public void setPickUp(String pickUp) {
        this.pickUp = pickUp;
    }

    public String getDropOff() {
        return dropOff;
    }

    public void setDropOff(String dropOff) {
        this.dropOff = dropOff;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getDropoutTime() {
        return dropoutTime;
    }

    public void setDropoutTime(String dropoutTime) {
        this.dropoutTime = dropoutTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
