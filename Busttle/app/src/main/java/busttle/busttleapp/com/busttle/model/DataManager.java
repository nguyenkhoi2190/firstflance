package busttle.busttleapp.com.busttle.model;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONObject;

import busttle.busttleapp.com.busttle.utils.SharePreference;
import busttle.busttleapp.com.busttle.utils.Utils;


public class DataManager {
    public static final String ADMIN_PASSWORD = "user_password";
    public static final String BUS_ID = "BUS_ID";
    public static final String DRIVER_NAME = "driver_name";
    public static final String DRIVER_EMAIL = "driver_email";
    public static final String DRIVER_PHONE = "driver_phone";
    public static final String DRIVER_TOKEN = "driver_token";

    private static DataManager mInstance;

    private Driver driver;

    public static DataManager getInstance() {
        if (mInstance == null) {
            mInstance = new DataManager();
        }

        return mInstance;
    }

    public DataManager() {

    }

    public Driver getDriver(Context context) {
        if (driver == null) {
            driver = new Driver();
        }

        driver.setName(SharePreference.getString(context, DRIVER_NAME));
        driver.setEmail(SharePreference.getString(context, DRIVER_EMAIL));
        driver.setPhoneNumber(SharePreference.getString(context, DRIVER_PHONE));
        driver.setAccessToken(SharePreference.getString(context, DRIVER_TOKEN));

        return driver;
    }

    public void saveAdminPassword(Context context, String password) {
        SharePreference.saveString(context, ADMIN_PASSWORD, password);
    }

    public String getAdminPassword(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SharePreference.SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        String defaultAdminPassword = Utils.md5("abc123");
        return sharedPreferences.getString(ADMIN_PASSWORD, defaultAdminPassword);
    }

    public void saveBusId(Context context, String busId) {
        SharePreference.saveString(context, BUS_ID, busId);
    }

    public String getBusId(Context context) {
//        return SharePreference.getString(context, BUS_ID);
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SharePreference.SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(BUS_ID, "1");
    }

    public void saveDriver(Context context, JSONObject jsonObject) {
        if (jsonObject != null) {
            String email = Parse.getString(jsonObject, "email");
            String name = Parse.getString(jsonObject, "name");
            String phone = Parse.getString(jsonObject, "phone_number");
            String token = Parse.getString(jsonObject, "authentication_token");

            SharePreference.saveString(context, DRIVER_EMAIL, email);
            SharePreference.saveString(context, DRIVER_NAME, name);
            SharePreference.saveString(context, DRIVER_PHONE, phone);
            SharePreference.saveString(context, DRIVER_TOKEN, token);
        }
    }


}
