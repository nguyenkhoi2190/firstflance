package busttle.busttleapp.com.busttle.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreference {
    public static final String SHARE_PREFERENCE_DATA = "share_preference_data";

    /**
     * save String
     */
    public static void saveString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * get String
     */
    public static String getString(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    /**
     * save Boolean
     */
    public static void saveBoolean(Context context, String key,
                                   boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * get Boolean
     */
    public static boolean getBoolean(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * save Integer
     */
    public static void saveInt(Context context, String key,	int value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * get Integer
     */
    public static int getInt(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, 0);
    }
}
