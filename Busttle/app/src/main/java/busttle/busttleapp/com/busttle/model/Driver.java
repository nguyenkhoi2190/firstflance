package busttle.busttleapp.com.busttle.model;

public class Driver {

    private String email;
    private String name;
    private String phoneNumber;
    private String accessToken;

    public Driver() {
    }

    public Driver(String email, String name, String phoneNumber, String accessToken) {
        this.email = email;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.accessToken = accessToken;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
